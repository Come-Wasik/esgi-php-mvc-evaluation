DROP TABLE IF EXISTS posts;

DROP TABLE IF EXISTS users;

-- Les utilisateurs --
CREATE TABLE users (
    id serial PRIMARY KEY NOT NULL,
    name varchar(50) NOT NULL UNIQUE
);

-- Les posts --
CREATE TABLE posts (
    id serial PRIMARY KEY NOT NULL,
    title varchar(30) NOT NULL,
    author int NOT NULL,
    CONSTRAINT fk_author_equal_user_id FOREIGN KEY (author) REFERENCES users (id)
);

INSERT INTO users (name)
    VALUES ('Grégoire'), ('Anthony');

INSERT INTO posts (title, author)
    VALUES ('My first post', (
            SELECT
                id
            FROM
                users
            WHERE
                name = 'Grégoire')),
    ('My second post',
        (
            SELECT
                id
            FROM
                users
            WHERE
                name = 'Grégoire')),
    ('My first post',
        (
            SELECT
                id
            FROM
                users
            WHERE
                name = 'Anthony'));

