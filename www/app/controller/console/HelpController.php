<?php

namespace App\Controller\Console;

use Framework\Architecture\Console\Router\RouteManager;

class HelpController
{
    public function showHelp()
    {
        echo 'The command entered doesn\'t exist. Command list :';
        foreach (RouteManager::getCommandList() as $command) {
            if ($command->getRegex() != '--help' && $command->getRegex() != '-h') {
                echo "\r\n" . "\t" . $command->getRegex();
            }
        }
        echo "\n";
    }
}
