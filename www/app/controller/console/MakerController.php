<?php

namespace App\Controller\Console;

class MakerController
{
    public function makeController(string $controllerFullname)
    {
        # Allow to control namespace with array
        $explodedController = explode('/', $controllerFullname);

        $controllerNamespace = array_merge( ## Adding App/Controller in the namespace
            ['App', 'Controller'],
            array_slice( ## Deleting the controller name
                array_map(function ($entry) { # Change all name in PascalCase
                    return ucfirst($entry);
                }, $explodedController), 0, count($explodedController) - 1)
        );
        # Comment : The class name and namespace should be in PascalCase
        $controllerOnlyName = ucfirst(array_slice($explodedController, -1, 1)[0]);

        # Write the file in a stream
        $fileContent = "<?php\r\n"
        . "\r\n"
        . 'namespace ' . implode('\\', $controllerNamespace) . ";\r\n"
        . "\r\n"
        . 'class ' . $controllerOnlyName . "Controller\r\n"
        . "{\r\n"
        . "\r\n"
        . "}\r\n"
        . "\r\n";

        # generate the controller path with the namespace #
        $controllerDir = controller_path() . '/' . implode( # Get the namespace with / separator
            '/',
            array_map(
                function ($entry) {
                    return lcfirst($entry);
                },
                array_slice( ## Select only controller namespace
                    $controllerNamespace,
                    2 ## Deleting the App/Controller part of the namespace
                )
            )
        );

        # Generate the directory if not exists #
        if (!file_exists($controllerDir)) {
            mkdir($controllerDir);
        }

        # Generate the file #
        file_put_contents($controllerDir . '/' . $controllerOnlyName . 'Controller.php', $fileContent);
        echo 'The controller ' . $controllerFullname . ' has been created.' . "\n";
    }

    public function makeEntity(string $entityName, string $entityAttributes = 'id:int')
    {
        # Change here the entityDirectory
        $entityDirectory = entity_path();

        # The class name should be in PascalCase
        $entityName = ucfirst($entityName);

        # Filtering of ; at the beginning and end so as not to write an empty value in the file later on
        $entityAttributes = trim($entityAttributes, ';"');

        # Forced adding for id
        $idIsPresent = false;
        $allAttrs = explode(';', $entityAttributes);
        for ($i = 0; $i < count($allAttrs) && $idIsPresent == false; $i++) {
            $attrData = explode(':', $allAttrs[$i]);
            if ($attrData[0] == 'id') {
                $idIsPresent = true;
            }
        }
        if ($idIsPresent === false) {
            $entityAttributes = 'id:int;' . $entityAttributes;
        }

        $getAttrType = function ($attribute) {
            $attribute = trim($attribute, ',;:');
            if (count($temp = explode(':', $attribute)) == 2) {
                return [
                    $temp[0],
                    $temp[1]
                ];
            }
            return [
                $temp[0],
                null
            ];
        };

        $fileContent = "<?php\r\n"
        . "\r\n"
        . "namespace App\\Entity;\r\n"
        . "\r\n"
        . 'class ' . $entityName . "\r\n"
        . "{\r\n";

        # Properties definition
        foreach (explode(';', $entityAttributes) as $attribute) {
            $attribute = trim($attribute);
            list($attribute, $attrType) = $getAttrType($attribute);

            $fileContent .= "\tprivate $" . $attribute . ";\r\n";
        }

        # getters definition
        $fileContent .= "\r\n";
        foreach (explode(';', $entityAttributes) as $attribute) {
            $attribute = trim($attribute);
            list($attribute, $attrType) = $getAttrType($attribute);

            $fileContent .= "\tpublic function get" . ucfirst($attribute) . '()' . ($attrType !== null ? ': ?' . $attrType : '') . "\r\n"
            . "\t{\r\n"
            . "\t\treturn \$this->" . $attribute . ";\r\n"
            . "\t}\r\n";
        }

        # Setters definition
        $fileContent .= "\r\n";
        foreach (explode(';', $entityAttributes) as $attribute) {
            $attribute = trim($attribute);
            list($attribute, $attrType) = $getAttrType($attribute);

            $fileContent .= "\tpublic function set" . ucfirst($attribute) . '(' . ($attrType !== null ? $attrType . ' ' : '') . '$' . $attribute . "): void\r\n"
            . "\t{\r\n"
            . "\t\t\$this->" . $attribute . ' = $' . $attribute . ";\r\n"
            . "\t}\r\n";
        }
        $fileContent .= "}\r\n"
        . "\r\n";

        # Generate the directory if not exists #
        if (!file_exists($entityDirectory)) {
            mkdir($entityDirectory);
        }

        # Generate the file #
        file_put_contents($entityDirectory . '/' . $entityName . '.php', $fileContent);
        echo 'The entity ' . $entityName . ' has been created.' . "\n";
    }

    public function makeValidator(string $validatorName)
    {
        $validatorPath = validator_path();

        $fileContent = "<?php\r\n"
        . "\r\n"
        . "return [\r\n"
        . "\t'metadata' => [\r\n"
        . "\r\n"
        . "\t],\r\n"
        . "\t'fields' => [\r\n"
        . "\t\t[\r\n"
        . "\t\t\t'name' => 'chooseYouWords',\r\n"
        . "\t\t\t'fieldType' => 'chooseYouType',\r\n"
        . "\t\t],\r\n"
        . "\t],\r\n"
        . "];\r\n"
        . "\r\n";

        # Generate the directory if not exists #
        if (!file_exists($validatorPath) || is_dir($validatorPath)) {
            mkdir($validatorPath);
        }

        # Generate the file #
        file_put_contents($validatorPath . '/' . $validatorName . '.php', $fileContent);
        echo 'The validator ' . $validatorName . ' has been created.';
    }

    public function makeTest(string $testName)
    {
        $testName = ucfirst($testName);
        $testDirectory = test_path();

        $fileContent = "<?php\r\n"
        . "\r\n"
        . "namespace Framework\\Test;\r\n"
        . "\r\n"
        . "use Exception;\r\n"
        . "\r\n"
        . 'class ' . $testName . "\r\n"
        . "{\r\n"
        . "\tpublic function tryMe()\r\n"
        . "\t{\r\n"
        . "\t\t# Write yours tests here and launch the command run:test " . $testName . " tryMe\r\n"
        . "\t}\r\n"
        . "}\r\n";

        # Generate the directory if not exists #
        if (!file_exists($testDirectory) || is_dir($testDirectory)) {
            mkdir($testDirectory);
        }

        # Generate the file #
        file_put_contents($testDirectory . '/' . $testName . '.php', $fileContent);
        echo 'The test ' . $testName . ' has been created.';
    }

    public function makeClass(string $className, string $classAttributes = null)
    {
        # Class directories #
        $classDir = app_path() . '/component';

        # The class name should be in PascalCase
        $className = ucfirst($className);

        # Filtering of ; at the beginning and end so as not to write an empty value in the file later on
        $classAttributes = trim($classAttributes, ';"');

        $getAttrType = function ($attribute) {
            $attribute = trim($attribute, ',;:');
            if (count($temp = explode(':', $attribute)) == 2) {
                return [
                    $temp[0],
                    $temp[1]
                ];
            }
            return [
                $temp[0],
                null
            ];
        };

        $fileContent = "<?php\r\n"
        . "\r\n"
        . "namespace App\\Component;\r\n"
        . "\r\n"
        . 'class ' . $className . "\r\n"
        . "{\r\n";

        # Properties definition
        if ($classAttributes) {
            foreach (explode(';', $classAttributes) as $attribute) {
                $attribute = trim($attribute);
                list($attribute, $attrType) = $getAttrType($attribute);

                $fileContent .= "\tprivate $" . $attribute . ";\r\n";
            }

            # getters definition
            $fileContent .= "\r\n";
            foreach (explode(';', $classAttributes) as $attribute) {
                $attribute = trim($attribute);
                list($attribute, $attrType) = $getAttrType($attribute);

                $fileContent .= "\tpublic function get" . ucfirst($attribute) . '()' . ($attrType !== null ? ': ?' . $attrType : '') . "\r\n"
                . "\t{\r\n"
                . "\t\treturn \$this->" . $attribute . ";\r\n"
                . "\t}\r\n";
            }

            # Setters definition
            $fileContent .= "\r\n";
            foreach (explode(';', $classAttributes) as $attribute) {
                $attribute = trim($attribute);
                list($attribute, $attrType) = $getAttrType($attribute);

                $fileContent .= "\tpublic function set" . ucfirst($attribute) . '(' . ($attrType !== null ? $attrType . ' ' : '') . '$' . $attribute . "): void\r\n"
                . "\t{\r\n"
                . "\t\t\$this->" . $attribute . ' = $' . $attribute . ";\r\n"
                . "\t}\r\n";
            }
        } else {
            $fileContent .= "\t\r\n";
        }

        # End of file
        $fileContent .= "}\r\n";

        # Generate the directory if not exists #
        if (!file_exists($classDir)) {
            mkdir($classDir);
        }

        # Generate the file #
        file_put_contents($classDir . '/' . $className . '.php', $fileContent);
        echo 'The class ' . $className . ' has been created.' . "\n";
    }
}
