<?php

namespace App\Controller\Console;

class RunnerController
{
    public function runTest(string $TestGroupName, string $testName)
    {
        # The group is in reality the class name
        $TestClassName = ucfirst($TestGroupName);
        $TestFullClassName = 'Framework\\Test\\' . $TestClassName;
        $testPath = base_path() . '/ressources/packages/tests';

        # Tests to know if the test can be launch
        require $testPath . '/' . $TestClassName . '.php';
        if (!class_exists($TestFullClassName)) {
            die('Class/TestGroupName ' . $TestFullClassName . ' don\'t exists');
        }
        if (!method_exists($TestFullClassName, $testName)) {
            die('Method/Test ' . $testName . ' don\'t exists');
        }

        # Launch the test
        (new $TestFullClassName())->$testName();

        echo 'The test has been correctly launched' . "\n";
    }
}
