<?php

namespace App\Entity;

class Post
{
    private $id;
    private $title;
    private $author;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getAuthor(): ?int
    {
        return $this->author;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setAuthor(int $author): void
    {
        $this->author = $author;
    }

    public function initRelation(): array
    {
        ### Voir /www/config/dataStorage/database.php
        return getConfig('db_relations')['Post'];
    }
}
