<?php

use Framework\Architecture\All\Component\Backpack\Config as ConfigBackpack;
use Framework\Architecture\All\Component\Backpack\Package as PackageBackpack;
use Framework\Architecture\Web\Component\Session;
use Framework\Architecture\Web\View\ViewManager;
use Framework\Architecture\Web\CoreMaker\Component\Http\Response;

if (!function_exists('setConfig')) {
    function setConfig($dataName, $dataContent)
    {
        return ConfigBackpack::set($dataName, $dataContent);
    }
}

if (!function_exists('getConfig')) {
    function getConfig($dataName)
    {
        return ConfigBackpack::get($dataName);
    }
}

if (!function_exists('setSession')) {
    function setSession($dataName, $dataContent)
    {
        $sessionManager = new Session();
        return $sessionManager->set($dataName, $dataContent);
    }
}

if (!function_exists('getSession')) {
    function getSession($dataName)
    {
        $sessionManager = new Session();
        return $sessionManager->get($dataName);
    }
}

if (!function_exists('session_stop')) {
    function session_stop()
    {
        $sessionManager = new Session();
        return $sessionManager->session_stop();
    }
}

if (!function_exists('response')) {
    function response(...$data)
    {
        return new Response(...$data);
    }
}

if (!function_exists('viewExists')) {
    function viewExists($viewPath)
    {
        return file_exists(ConfigBackpack::get('view_path') . '/' . $viewPath);
    }
}

if (!function_exists('view')) {
    function view($viewName, $variables = [])
    {
        return (new ViewManager())->show($viewName, $variables);
    }
}

if (!function_exists('setDependency')) {
    function setDependency($dataName, $dataContent)
    {
        return PackageBackpack::set($dataName, $dataContent);
    }
}

if (!function_exists('getPackage')) {
    function getPackage($dataName)
    {
        return PackageBackpack::get($dataName);
    }
}

if (!function_exists('getInitializedPackage')) {
    function getInitializedPackage($dataName)
    {
        $package = PackageBackpack::get($dataName);
        if (key_exists('newInstance', $package)) {
            return $package['newInstance']();
        }
        return null;
    }
}

if (!function_exists('redirection')) {
    function redirection($redirectionPath)
    {
        header('Location: ' . rtrim(WEBSITE_URL, '/') . '/' . ltrim($redirectionPath, '/'));
        exit();
    }
}
