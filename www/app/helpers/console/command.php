<?php

use Framework\Architecture\Console\Router\RouteManager;
use Framework\Architecture\Console\Router\Route;

class Command
{
    public static function add(string $regexToRespect, $action)
    {
        $manager = new RouteManager();
        $manager->addCommand(new Route([
            'regex' => $regexToRespect,
            'action' => $action
        ]));
    }
}
