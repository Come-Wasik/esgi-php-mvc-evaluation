<?php

use Framework\Architecture\Console\CoreMaker\ConsoleMaker;
use Framework\Architecture\Console\CoreMaker\Stream\Command as Request;

if (!function_exists('use_console')) {
    function use_console(string $command)
    {
        $command = new Request(['command' => $command]);

        $consoleMaker = new ConsoleMaker();
        $consoleMaker->launchCommand($command);
    }
}
