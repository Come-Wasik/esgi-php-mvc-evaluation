<?php

if (!function_exists('get_method_args')) {
    function get_method_args($controller, $method)
    {
        $methodArgs = [];

        $refclass = new ReflectionClass($controller);
        $refMethod = $refclass->getMethod($method);
        $refArgs = $refMethod->getParameters();

        foreach ($refArgs as $refArg) {
            $methodArgs[$refArg->getName()] = [
                'name' => $refArg->getName(),
                'type' => ($refArg->getType() !== null ? $refArg->getType()->__tostring() : null),
            ];
        }
        return $methodArgs;
    }
}

if (!function_exists('get_callback_args')) {
    function get_callback_args($callback)
    {
        $callbackArgs = [];

        $refMethod = new ReflectionFunction($callback);
        $refArgs = $refMethod->getParameters();

        foreach ($refArgs as $refArg) {
            $callbackArgs[$refArg->getName()] = [
                'name' => $refArg->getName(),
                'type' => ($refArg->getType() !== null ? $refArg->getType()->__tostring() : null),
            ];
        }
        return $callbackArgs;
    }
}
