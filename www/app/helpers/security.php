<?php

use Framework\Architecture\Web\Security\Authentification;

if (!function_exists('userIsConnected')) {
    function userIsConnected(): bool
    {
        $authManager = new Authentification();
        return $authManager->userIsConnnected();
    }
}

if (!function_exists('getUserRole')) {
    function getUserRole(): ?string
    {
        $token = getSession('session_token');
        $tokenDecoded = \json_decode($token, 1);

        return $tokenDecoded['payload']['role'];
    }
}

if (!function_exists('getUserId')) {
    function getUserId(): ?int
    {
        $token = getSession('session_token');
        $tokenDecoded = \json_decode($token, 1);

        return $tokenDecoded['payload']['id'];
    }
}
