<?php

return [
    'makerIni' => [
        'importantData' => [
            'system_path',
            'config_path',
            'data_storage_path'
        ],
        'importantPath' => [
            'system_path',
            'config_path',
            'data_storage_path'
        ]
    ],
    'autoloaderPath' => '/autoloader',
    'bootstrap' => [
        'bootstrapFilePath' => '/framework/bootstrap/main.php',
        'bootstrapFunctionFilePath' => '/framework/bootstrap/ressources/functions.php'
    ],
    'config' => [
        'importantConfigData' => [
            'app_path',
            'controller_path',
            'helper_path',
            'route_path',
            'view_path',
            'router',
            'errorManaging',
        ],
        'importantConfigFilePath' => [
            'path',
            'router',
            'errorManaging',
        ]
    ],
    'helper' => [
        'importantHelpersFunction' => [
            'setConfig',
            'getConfig',
            'app_path',
            'controller_path',
            'route_path',
            'view_path',
            'config_path',
            'data_storage_path',
            'viewExists',
            'view'
        ],
        'recommandedHelpersFunction' => [
            'use_console',
            'use_web',
            'setSession',
            'getSession',
            'response',
            'validator_path',
            'createForm',
            'addFormField'
        ],
        'importantHelpersClasses' => [
            'Command' => [
                'add'
            ],
            'NewRoute' => [
                'get',
                'post',
                'mixed'
            ]
        ],
        'recommandedHelpersClasses' => [

        ]
    ]
];