<?php

return [
    'app' => [
        'default_timezone' => 'Europe/Paris'
    ]
];