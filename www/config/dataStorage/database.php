<?php

use App\Entity\User;

return [
    'db_driver' => DB_DRIVER ?? 'pgsql',
    'db_host' => DB_HOST ?? '127.0.0.1',
    'db_name' => DB_NAME ?? 'myDb',
    'db_user' => DB_USER ?? 'postgres',
    'db_password' => DB_PASSWORD ?? 'postgres',

    ### Define all relations for entities
    'db_relations' => [
        'Post' => [
            'author' => User::class
        ]
    ]
];
