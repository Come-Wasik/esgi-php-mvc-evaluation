<?php

return [
    'security' => [
        'moduleEnabled' => false,
        'userEntity' => 'App\Entity\Users',
        'roleEntity' => 'App\Entity\User_roles',
        'connexionData' => [
            'route' => '/login',
            'httpMethod' => 'POST',
            'redirectionAfterProcessing' => '/',
        ],
        'registrationData' => [
            'route' => '/register',
            'httpMethod' => 'POST',
            'redirectionAfterProcessing' => '/',
            'connexionAfterRegistration' => false,
        ],
        'deconnexionData' => [
            'route' => '/deconnexion',
            'redirectionAfterProcessing' => '/',
        ],
    ],
];
