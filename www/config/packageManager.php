<?php

use Framework\Architecture\All\Component\Database\DatabaseManager;
use Framework\Architecture\All\Component\Database\PDOConnect;

return [
    # name your class
    'DatabaseManager' => [
        # Put all bootstrap actions
        'newInstance' => function () {
            $database = new DatabaseManager(
                $dbDriver = getConfig('db_driver'),
                $dbHost = getConfig('db_host'),
                $dbName = getConfig('db_name'),
                $dbUser = getConfig('db_user'),
                $dbPassword = getConfig('db_password')
            );
            return $database;
        }
    ],
    'PDOConnect' => [
        # Put all bootstrap actions
        'newInstance' => function () {
            $pdoConnect = new PDOConnect();
            $pdoConnect->connexion(
                getConfig('db_driver'),
                getConfig('db_host'),
                getConfig('db_name'),
                getConfig('db_user'),
                getConfig('db_password')
            );
            return $pdoConnect;
        }
    ]
];
