<?php

/**
 * 
 * @project ReverseCallback
 * @author SecretCanvas
 */

use Framework\Architecture\Web\CoreMaker\WebsiteMaker;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

## Define architecture ##
define('ARCH', 'web');

### Including makerIni ###
$makeIniPath = dirname(__DIR__).'/maker.ini';
if(!file_exists($makeIniPath)) {
    die('Fatal error : makerIni file should exists');
}
$makerData = parse_ini_file($makeIniPath);

if(!isset($makerData['system_path'])) {
    die('Fatal error : makerIni file should comport a system_path data');
}

### Define system directory ###
$systemPath = dirname(__DIR__).'/'.$makerData['system_path'];

### Include autoloader ###
foreach(glob($systemPath.'/autoloader/*.php') as $autoloader) {
    include $autoloader;
}

### Include bootstrap ###
require $systemPath.'/framework/bootstrap/main.php';

### Get Request from globals ###
$request = new Request();
$request->fillFromGlobals();

### Load web maker and process the request ###
$webMaker = new WebsiteMaker();
$clientResponse = $webMaker->processRequest($request);

### Send web response ###
$webMaker->sendResponse($clientResponse);