<?php

return [
    'fields' => [
        'username' => [
            'fieldType' => 'text',
        ],
        'password' => [
            'fieldType' => 'text',
        ],
        'passwordConfirm' => [
            'fieldType' => 'text',
        ],
        'email' => [
            'fieldType' => 'email',
        ],
        'firstname' => [
            'fieldType' => 'text',
        ],
        'lastname' => [
            'fieldType' => 'text',
        ],
    ],
];
