<?php ob_start() ?>
<div class="container py-20">
    <div class="row">
        <div class="col-md-4">
            <aside class="bg-white max-w-xs shadow-lg px-6 py-8">
                <div class="flex items-center justify-center mt-3">
                    <svg class="w-8 h-8 text-svg-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M12.3 3.7l4 4L4 20H0v-4L12.3 3.7zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/>
                    </svg>
                </div>
                <div class="text-center">
                    <h5 class="text-2xl font-medium text-black">12, 334</h5>
                    <h5 class="text-xl font-medium text-gray-4">Posts</h5>
                </div>
            </aside>
        </div>
        <div class="col-md-4">
            <aside class="bg-white max-w-xs shadow-lg px-6 py-8">
                <div class="flex items-center justify-center mt-3">
                    <svg class="w-8 h-8 text-svg-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M16 2h4v15a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V0h16v2zm0 2v13a1 1 0 0 0 1 1 1 1 0 0 0 1-1V4h-2zM2 2v15a1 1 0 0 0 1 1h11.17a2.98 2.98 0 0 1-.17-1V2H2zm2 8h8v2H4v-2zm0 4h8v2H4v-2zM4 4h8v4H4V4z"/>
                    </svg>
                </div>
                <div class="text-center">
                    <h5 class="text-2xl font-medium text-black">12, 334</h5>
                    <h5 class="text-xl font-medium text-gray-4">Pages</h5>
                </div>
            </aside>
        </div>

        <div class="col-md-4">
            <aside class="bg-white max-w-xs shadow-lg px-6 py-8">
                <div class="flex items-center justify-center mt-3">
                    <svg class="w-8 h-8 text-svg-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"/>
                    </svg>
                </div>
                <div class="text-center">
                    <h5 class="text-2xl font-medium text-black">12, 334</h5>
                    <h5 class="text-xl font-medium text-gray-4">Utilisateurs</h5>
                </div>
            </aside>
        </div>
    </div>
</div>

<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
