<?php ob_start() ?>
<div class="container py-12">
    <div class="row">
        <div class="col-md-4">
            <h1 class="text-md font-normal">Modifier le tag <?= $tag->getName() ?></h1>
        </div>
        <div class="col-md-8">
            <form class="w-full pt-6 pb-8" method="POST">
                <div class="flex items-center justify-center mb-5">
                    <div class="form-group mb-5 mr-5">
                        <label for="" class="text-black">Nom du tag</label>
                        <input type="text" class="" name="name" placeholder="Nommez votre tag" value="<?= $tag->getName() ?>" required>
                    </div>
                </div>
                <div class="flex items-center justify-center mb-5">
                    <div class="form-group mr-5">
                        <label for="" class="">Description du tag</label>
                        <input type="text" class="" name="description" placeholder="Optionnel... Entrez une courte description..." value="<?= $tag->getDescription() ?>">
                    </div>
                </div>

                <div class="flex items-center justify-end my-12">
                    <button class="button button-big button--bg-success" type="submit">Ajouter le tag</button>
                </div>
            </form>
        </div>

        <?php if ($errorMessage): ?>
            Erreur : <?= $errorMessage ?>
        <?php endif; ?>
    </div>
</div>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
