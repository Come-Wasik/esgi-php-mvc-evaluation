<?php ob_start() ?>
    <div class="container py-12">
        <div class="flex items-center justify-between">
            <h1 class="text-blue-3 text-2xl font-medium"><span class="border-b-2 border-blue-3">Gestions</span> des tags</h1>
            <a href="<?= getRouteUrl('tags.create') ?>">
                <svg class="text-svg-blue-3 w-6 h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M11 9h4v2h-4v4H9v-4H5V9h4V5h2v4zm-1 11a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16z"/>
                </svg>
            </a>
        </div>
        <section>
            <table class="w-full" style="border-collapse: collapse">
                <thead>
                    <tr class="text-left border-b">
                        <th class="pr-4 py-2">Id</th>
                        <th class="px-4 py-2">Nom du tag</th>
                        <th class="px-4 py-2">Description</th>
                        <th class="px-4 py-2">Action</th>
                    </tr>
                </thead>
                <tbody class="">
                <?php foreach ($tags as $tag): ?>
                    <tr class="border-b">
                        <td class="pr-4 py-3"><?= $tag->getId() ?></td>
                        <td class="px-4 py-3"><?= $tag->getName() ?></td>
                        <td class="px-16 py-3"><?= $tag->getDescription() ?></td>
                        <td class="px-4 py-3">
                            <div class="flex items-center">
                                <form method="POST" action="<?= getRouteUrl('tags.delete') ?>">
                                    <button type="submit" name="id_tag" value="<?= $tag->getId() ?>" class="no-button" style="width: 40px;">
                                        <svg class="w-4 h-4 text-svg-danger mr-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                            <path d="M6 2l2-2h4l2 2h4v2H2V2h4zM3 6h14l-1 14H4L3 6zm5 2v10h1V8H8zm3 0v10h1V8h-1z"/>
                                        </svg>
                                    </button>
                                </form>
                                <a href="<?= getRouteUrl('tags.edit') . $tag->getId() ?>" class="block">
                                    <svg class="w-4 h-4 text-svg-primary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"/>
                                    </svg>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </section>
    </div>

<?php $_sectionContent = ob_get_clean();
require view_path() . '/dashboard/template.php';
