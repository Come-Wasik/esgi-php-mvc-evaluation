<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>CMS | Création de blog</title>
    <link rel="stylesheet" href="/css/main.css">
</head>
<body class="container-full">
    <div class="row">
        <nav class="col-sm-6 col-md-2 col-lg-2 h-screen shadow-r-inset">
            <div class="container h-full">
                <div class="row align-center h-full">
                    <nav class="col-md-10 h-full dashboard">
                        <a href="#" class="title">Blogify</a>
                        <a href="<?= getRouteUrl('dashboard') ?>" class="">Dashboard</a>
                        <a href="<?= getRouteUrl('pages.index') ?>" class="">Pages</a>
                        <a href="<?= getRouteUrl('articles.index') ?>" class="">Articles</a>
                        <a href="<?= getRouteUrl('categories.index') ?>" class="">Catégories</a>
                        <a href="<?= getRouteUrl('tags.index') ?>" class="">Tags</a>
                        <a href="<?= getRouteUrl('comments.index') ?>" class="">Commentaires</a>
                        <a href="<?= getRouteUrl('users.index') ?>" class="">Utilisateurs</a>
                        <a href="<?= getRouteUrl('articles.index') ?>" class="">Thèmes</a>
                        <a href="<?= getRouteUrl('articles.index') ?>" class="">Paramètres</a>
                    </nav>
                </div>
            </div>
        </nav>
        <section class="col-sm-6 col-md-10 col-lg-10 bg-gray-2 h-screen">
            <header class="row bg-white py-3 shadow-b">
                <div class="col-md-12">
                    <div class="flex items-center justify-end px-16 py-2">
                        <span class="text-sm font-medium mr-1">John Doe</span>
                        <svg class="w-4 h-4 text-svg-danger" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M4.16 4.16l1.42 1.42A6.99 6.99 0 0 0 10 18a7 7 0 0 0 4.42-12.42l1.42-1.42a9 9 0 1 1-11.69 0zM9 0h2v8H9V0z"/>
                        </svg>
                    </div>
                </div>
            </header>
            <main class="row">
                <div class="col-md-12">
                    <?= $_sectionContent ?>
                </div>
            </main>
        </section>
    </div>
</body>
</html>