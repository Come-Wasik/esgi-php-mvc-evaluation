<?php ob_start() ?>
    <section class="bg-blue-3 px-32 py-8">
        <h1 class="mx-32 ml-32 text-white text-4xl font-semibold mt-0"><span class="border-b-2 ml-28">Nos</span> articles <span class="text-normal">(3)</span></h1>
    </section>
    <section class="container">
        <div class="p-10">
            <?php foreach ($articles as $article): ?>
            <div class="row my-6">
                <aside class="col-sm-3 col-md-3 col-lg-3">
                    <h5 class="text-black text-normal font-medium mb-1">By
                        <?php foreach ($users as $user) {
                            echo ($user->getId() === $article->getAuthor_id()) ? $user->getUsername() : 'ARB';
                        } ?>
                    </h5>
                    <span class="block text-gray-3 text-sm">3 days ago</span>
                </aside>
                <article class="col-sm-9 col-md-9 col-lg-9 border-b border-gray-1">
                    <h4 class="text-xl font-medium"><a href="<?= getRouteUrl('posts.show') . $article->getId() ?>" class="no-underline text-black"><?= $article->getName() ?></a></h4>
                    <p class="text-md font-200">
                        <?= $article->getDescription() ?>
                    </p>
                    <a href="<?= getRouteUrl('posts.show') . $article->getId() ?>" class="block no-underline text-black text-lg mb-6">&rarr;</a>
                </article>
            </div>
            <?php endforeach; ?>
            <section class="flex items-center justify-center mt-32 mb-0">
                <button class="button button-big button--bg-primary">Voir plus</button>
            </section>
        </div>
    </section>
<?php $_sectionContent = ob_get_clean();
require view_path() . '/front/template.php';
