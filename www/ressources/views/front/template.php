<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Les articles | Blogify</title>
    <link rel="stylesheet" href="../../../css/main.css">
</head>
<body class="container-full flex flex-col h-screen">
    <header class="col-sm-12 col-md-12 col-lg-12 bg-blue-3" style="flex-basis: initial;">
        <nav class="container">
            <div class="flex items-center p-6">
                <a href="<?= getRouteUrl('posts.index') ?>" class="text-2xl font-semibold text-white no-underline mr-32">Blogify</a>
                <a href="#" class="text-lg text-white no-underline mr-4 px-2 py-2 ml-16">Accueil</a>
                <a href="#" class="text-lg text-white no-underline mr-4 px-2 py-2">Articles</a>
                <a href="#" class="text-lg text-white no-underline mr-4 px-2 py-2">A propos</a>
            </div>
        </nav>
    </header>
    <main class="bg-personal-color-1 flex-1">
        <?= $_sectionContent ?>
    </main>
</body>
</html>