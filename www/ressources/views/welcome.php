<!DOCTYPE html>
<html>
    <head>
        <title>Main page</title>
        <meta charet="utf-8" />
        <style>
            body { margin: 0px; background-color: rgb(237, 255, 195); }
            main {
                margin: 3% 4em;
                color: rgb(83, 54, 0);
                background-color: rgb(243, 255, 215);
            }
            h1 { font-size: 48px; font-family : arial; margin: 0px; color: rgb(255, 76, 44); }
            h1 + p { margin-top: 35px;}
            p, li { font-size: 28px; }
            p + ul { margin-top: -12px;}
            .box {
                padding: 2em; box-sizing: border-box;
                max-width: 1000px;
                margin-left: auto;  margin-right: auto;
            }
            .center-align { text-align: center;}
        </style>
    </head>
    <body>
        <main>
            <div class="box">
                <h1 class="center-align">Welcome to our Framework !</h1>
                <p>You're now ready to use this mini Framework.</p>

                <p>
                <?php if (userIsConnected()): ?>
                    Vous êtes connecté. Votre rôle : <?= getUserRole() ?>
                <?php else: ?>
                    Vous n'êtes pas connecté.
                <?php endif; ?>
                </p>
            </div>
        </main>
    </body>
</html>