<?php

# Routes qui sont requises pour afficher du contenu classique à l'utilisateur

use App\Controller\HomeController;

NewRoute::get('app.homepage', '/', HomeController::class . '@index');
