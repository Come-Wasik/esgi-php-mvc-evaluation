<?php

spl_autoload_register(function($className) {
    ### Define the system directory path ###
    ## Including makerIni ##
    $makeIniPath = dirname(dirname(__DIR__)).'/maker.ini';
    if(!file_exists($makeIniPath)) {
        die('Fatal error : makerIni file should exists');
    }
    $makerData = parse_ini_file($makeIniPath);

    if(!isset($makerData['system_path'])) {
        die('Fatal error : makerIni file should comport a system_path data');
    }

    ## Define system directory ##
    $systemPath = dirname(dirname(__DIR__)).'/'.$makerData['system_path'];

    ### Extract namespace and classe name ###
    $namespaceParts = explode('\\', $className);
    $justClassName = array_pop($namespaceParts);
    $justNamespace = implode('\\', $namespaceParts);

    if(!empty($justNamespace)) {
        ## Has namespace ##
        switch($justNamespace) {
            # Package - Logger
            case 'Packages\Logger':
                $classPath = $systemPath.'/logger/'.$justClassName.'.php';
                include $classPath;
                break;

            # Package - FormMaker
            case 'Packages\FormMaker\Builder':
            $classPath = $systemPath.'/formMaker/builder/'.$justClassName.'.php';
            include $classPath;
            break;

            case 'Packages\FormMaker\Builder\Component\Field':
            $classPath = $systemPath.'/formMaker/builder/fields/'.$justClassName.'.php';
            include $classPath;
            break;

            case 'Packages\FormMaker\Validator':
            $classPath = $systemPath.'/formMaker/validator/'.$justClassName.'.php';
            include $classPath;
            break;
            case 'Packages\FormMaker\Validator\Exception':
                $classPath = $systemPath.'/formMaker/validator/exception/'.$justClassName.'.php';
                include $classPath;
                break;
        }
    } else {
        ## Has not namespace ##
        switch($justClassName) {

        }
    }
});
