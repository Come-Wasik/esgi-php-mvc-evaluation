<?php

spl_autoload_register(function ($className) {
    ### Define the system directory path ###
    ## Including makerIni ##
    $makeIniPath = dirname(dirname(__DIR__)) . '/maker.ini';
    if (!file_exists($makeIniPath)) {
        die('Fatal error : makerIni file should exists');
    }
    $makerData = parse_ini_file($makeIniPath);

    if (!isset($makerData['system_path'])) {
        die('Fatal error : makerIni file should comport a system_path data');
    }

    ## Define system directory ##
    $systemPath = dirname(dirname(__DIR__)) . '/' . $makerData['system_path'];

    ### Extract namespace and classe name ###
    $namespaceParts = explode('\\', $className);
    $justClassName = array_pop($namespaceParts);
    $justNamespace = implode('\\', $namespaceParts);

    if (!empty($justNamespace)) {
        ## Has namespace ##
        switch ($justNamespace) {
            # Bootstrap - Fileloaders
            case 'Framework\Bootstrap\FileLoader':
                $classPath = $systemPath . '/framework/bootstrap/fileloaders/' . $justClassName . '.php';
                include $classPath;
                break;

            # All - Backpacks
            case 'Framework\Architecture\All\Component\Backpack':
                $classPath = $systemPath . '/framework/architecture/.global/backpack/' . $justClassName . '.php';
                include $classPath;
                break;

            # All - Interfaces
            case 'Framework\Architecture\All\Component\Interfaces':
            $classPath = $systemPath . '/framework/architecture/.global/interfaces/' . $justClassName . '.php';
            include $classPath;
            break;

            # All - Interfaces
            case 'Framework\Architecture\All\Component\Dependency':
                $classPath = $systemPath . '/framework/architecture/.global/dependency/' . $justClassName . '.php';
                include $classPath;
                break;

            # All - Models
            case 'Framework\Architecture\All\Component\Database':
                $classPath = $systemPath . '/framework/architecture/.global/model/' . $justClassName . '.php';
                include $classPath;
                break;
            case 'Framework\Architecture\All\Component\Database\Ressource':
                $classPath = $systemPath . '/framework/architecture/.global/model/ressources/' . $justClassName . '.php';
                include $classPath;
                break;

            # All - Exception
            case 'Framework\Architecture\All\Component\Exception':
                $classPath = $systemPath . '/framework/architecture/.global/exception/' . $justClassName . '.php';
                include $classPath;
                break;

            # Console - Router
            case 'Framework\Architecture\Console\Router':
                $classPath = $systemPath . '/framework/architecture/console/router/' . $justClassName . '.php';
                include $classPath;
                break;

            # Console - Core
            case 'Framework\Architecture\Console\CoreMaker':
                $classPath = $systemPath . '/framework/architecture/console/core/' . $justClassName . '.php';
                include $classPath;
                break;

            # Console - Core - Stream
            case 'Framework\Architecture\Console\CoreMaker\Stream':
            $classPath = $systemPath . '/framework/architecture/console/core/stream/' . $justClassName . '.php';
            include $classPath;
            break;

            # Console - Backpacks
            case 'Framework\Architecture\Console\Component\Backpack':
            $classPath = $systemPath . '/framework/architecture/console/backpack/' . $justClassName . '.php';
            include $classPath;
            break;

            # Web - Components
            case 'Framework\Architecture\Console\Component':
            $classPath = $systemPath . '/framework/architecture/console/component/' . $justClassName . '.php';
            include $classPath;
            break;

            # Web - MVC - View
            case 'Framework\Architecture\Web\View':
                $classPath = $systemPath . '/framework/architecture/web/view/' . $justClassName . '.php';
                include $classPath;
                break;

            # Web - MVC - Router
            case 'Framework\Architecture\Web\Routage':
                $classPath = $systemPath . '/framework/architecture/web/router/' . $justClassName . '.php';
                include $classPath;
                break;

            # Web - Core - WebsiteMaker
            case 'Framework\Architecture\Web\CoreMaker':
                $classPath = $systemPath . '/framework/architecture/web/core/' . $justClassName . '.php';
                include $classPath;
                break;

            # Web - Core - Http
            case 'Framework\Architecture\Web\CoreMaker\Component\Http':
                $classPath = $systemPath . '/framework/architecture/web/core/http/' . $justClassName . '.php';
                include $classPath;
                break;

            # Web - Component
            case 'Framework\Architecture\Web\Component':
                $classPath = $systemPath . '/framework/architecture/web/component/' . $justClassName . '.php';
                include $classPath;
                break;

            # Web - Backpacks
                case 'Framework\Architecture\Web\Component\Backpack':
                $classPath = $systemPath . '/framework/architecture/web/backpack/' . $justClassName . '.php';
                include $classPath;
                break;

            # Web - Security
            case 'Framework\Architecture\Web\Security':
                $classPath = $systemPath . '/framework/architecture/web/security/' . $justClassName . '.php';
                include $classPath;
                break;
        }
    }
});
