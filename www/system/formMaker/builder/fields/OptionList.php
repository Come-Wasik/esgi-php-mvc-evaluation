<?php

namespace Packages\FormMaker\Builder\Component\Field;

use Exception;

class OptionList extends FormTag
{
    private $data;
    private $selected;

    public function getData(): ?array
    {
        return $this->data;
    }

    public function getSelected(): ?string
    {
        return $this->selected;
    }

    public function setData(array $data): void
    {
        if ($data == null || !is_array($data)) {
            throw new Exception('Data is empty or not an array ' . ($this->getName() != null ? 'in the ' . $this->getName() : 'in an') . ' option list', 500);
        }
        foreach ($data as $element) {
            if (!($element instanceof OptionElement)) {
                throw new Exception('One element ' . ($this->getName() != null ? 'in the ' . $this->getName() : 'in an') . ' option list is not an instance of FormOptionElement', 500);
            }
        }
        $this->data = $data;
    }

    public function setSelected(string $selected): void
    {
        $this->selected = $selected;
    }
}
