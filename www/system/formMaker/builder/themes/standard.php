<?php
    use Packages\FormMaker\Builder\Component\Field\{
        Input,
        OptionList,
        TextArea,
        Button
    };
?>

<form
    <?= ($this->getName() != null ? 'name="'.$this->getName().'"' : '') ?>
    <?= ($this->getId() != null ? 'id="'.$this->getId().'"' : '') ?>
    <?= ($this->getClass() != null ? 'class="'.$this->getClass().'"' : '') ?>
    <?= ($this->getMethod() != null ? 'method="'.$this->getMethod().'"' : '') ?>
    <?= ($this->getAction() != null ? 'action="'.$this->getAction().'"' : '') ?>
    <?= ($this->getEnctype() != null ? 'enctype="'.$this->getEnctype().'"' : '') ?>
    <?= ($this->getAccept_charset() != null ? 'accept-charset="'.$this->getAccept_charset().'"' : '') ?>
    <?= ($this->getAutocomplete() != null ? 'autocomplete="'.$this->getAutocomplete().'"' : '') ?>
    <?= ($this->getNovalidate() != null ? 'novalidate="'.$this->getNovalidate().'"' : '') ?>
    <?= ($this->getTarget() != null ? 'target="'.$this->getTarget().'"' : '') ?>
>

<?php if(!empty($this->getFieldList())): ?>
    <?php foreach($this->getFieldList() as $field): ?>
        <?php if($field instanceof Input): ?>
            <?= ($field->getLabel() != null
            ?   '<label for="'.($field->getId() != null ? $field->getId() : '').'">'
                .$field->getLabel()
                .'</label>'
            : '') ?>
            <input
                <?php ### Tier 1 attributes : FormField attributes per inheritance ### ?>
                <?= ($field->getName() != null ? 'name="'.$field->getName().'"' : '') ?>
                <?= ($field->getId() != null ? 'id="'.$field->getId().'"' : '') ?>
                <?= ($field->getClass() != null ? 'class="'.$field->getClass().'"' : '') ?>
                <?= ($field->getValue() != null ? 'value="'.$field->getValue().'"' : '') ?>
                <?= ($field->getDisabled() == true ? 'disabled="disabled"' : '') ?>
                
                <?php ### Tier 2 attributes : Input attributes ### ?>
                <?= ($field->getType() != null ? 'type="'.$field->getType().'"' : '') ?>
                <?= ($field->getAccept() != null ? 'accept="'.$field->getAccept().'"' : '') ?>
                <?= ($field->getAlt() != null ? 'alt="'.$field->getAlt().'"' : '') ?>
                <?= ($field->getAutocomplete() == true ? 'autocomplete="autocomplete"' : '') ?>
                <?= ($field->getAutofocus() == true ? 'autofocus="autofocus"' : '') ?>
                <?= ($field->getChecked() == true ? 'checked="checked"' : '') ?>
                <?= ($field->getDirname() != null ? 'dirname="'.$field->getDirname().'"' : '') ?>

                <?= ($field->getFormaction() != null ? 'formaction="'.$field->getFormaction().'"' : '') ?>
                <?= ($field->getFormenctype() != null ? 'formenctype="'.$field->getFormenctype().'"' : '') ?>
                <?= ($field->getFormmethod() != null ? 'formmethod="'.$field->getFormmethod().'"' : '') ?>
                <?= ($field->getFormnovalidate() != null ? 'formnovalidate="'.$field->getFormnovalidate().'"' : '') ?>
                <?= ($field->getFormtarget() != null ? 'formtarget="'.$field->getFormtarget().'"' : '') ?>

                <?= ($field->getMin() != null ? 'min="'.$field->getMin().'"' : '') ?>
                <?= ($field->getMax() != null ? 'max="'.$field->getMax().'"' : '') ?>
                <?= ($field->getStep() != null ? 'step="'.$field->getStep().'"' : '') ?>

                <?= ($field->getMinlenght() != null ? 'minlenght="'.$field->getMinlenght().'"' : '') ?>
                <?= ($field->getMaxlenght() != null ? 'maxlenght="'.$field->getMaxlenght().'"' : '') ?>
                <?= ($field->getMultiple() == true ? 'multiple="'.$field->getMultiple().'"' : '') ?>

                <?= ($field->getPattern() != null ? 'pattern="'.$field->getPattern().'"' : '') ?>
                <?= ($field->getPlaceholder() != null ? 'placeholder="'.$field->getPlaceholder().'"' : '') ?>
                <?= ($field->getReadonly() == true ? 'readonly="readonly"' : '') ?>
                <?= ($field->getRequired() == true ? 'required="required"' : '') ?>
                <?= ($field->getSrc() != null ? 'src="'.$field->getSrc().'"' : '') ?>
            />
        <?php elseif($field instanceof OptionList): ?>
            <select
                <?php ### Tier 1 attributes : FormField attributes per inheritance ### ?>
                <?= ($field->getName() != null ? 'name="'.$field->getName().'"' : '') ?>
                <?= ($field->getId() != null ? 'id="'.$field->getId().'"' : '') ?>
                <?= ($field->getClass() != null ? 'class="'.$field->getClass().'"' : '') ?>
                <?= ($field->getValue() != null ? 'value="'.$field->getValue().'"' : '') ?>
                <?= ($field->getDisabled() == true ? 'disabled="disabled"' : '') ?>

                <?php ### Tier 2 attributes : OptionList attributes ### ?>
                <?= ($field->getSelected() != null ? 'selected="'.$field->getSelected().'"' : '') ?>
            >
                <?php if($field->getData() != null): ?>
                    <?php foreach($field->getData() as $option): ?>
                        <option
                            <?= ($option->getName() != null ? 'name="'.$option->getName().'"' : '') ?>
                            <?= ($option->getId() != null ? 'id="'.$option->getId().'"' : '') ?>
                            <?= ($option->getClass() != null ? 'class="'.$option->getClass().'"' : '') ?>
                            <?= ($option->getValue() != null ? 'value="'.$option->getValue().'"' : '') ?>
                        >
                        <?= ($option->getLabel() != null ? $option->getLabel() : '') ?>
                        </option>
                    <?php endforeach ?>
                <?php endif; ?>
            </select>
        <?php elseif($field instanceof TextArea): ?>
            <textarea
                <?php ### Tier 1 attributes : FormField attributes per inheritance ### ?>
                <?= ($field->getName() != null ? 'name="'.$field->getName().'"' : '') ?>
                <?= ($field->getId() != null ? 'id="'.$field->getId().'"' : '') ?>
                <?= ($field->getClass() != null ? 'class="'.$field->getClass().'"' : '') ?>
                <?= ($field->getDisabled() == true ? 'disabled="disabled"' : '') ?>

                <?php ### Tier 2 attributes : TextArea attributes ### ?>
                <?= ($field->getAutofocus() == true ? 'autofocus="autofocus"' : '') ?>
                <?= ($field->getCols() != null ? 'cols="'.$field->getCols().'"' : '') ?>
                <?= ($field->getDirname() != null ? 'dirname="'.$field->getDirname().'"' : '') ?>
                <?= ($field->getMaxlenght() != null ? 'maxlenght="'.$field->getMaxlenght().'"' : '') ?>

                <?= ($field->getPlaceholder() != null ? 'placeholder="'.$field->getPlaceholder().'"' : '') ?>
                <?= ($field->getReadonly() == true ? 'readonly="readonly"' : '') ?>
                <?= ($field->getRequired() == true ? 'required="required"' : '') ?>
                <?= ($field->getRows() != null ? 'rows="'.$field->getRows().'"' : '') ?>
                <?= ($field->getWrap() != null ? 'wrap="'.$field->getWrap().'"' : '') ?>

            ><?= ($field->getValue() != null ? $field->getValue() : '') ?></textarea>
        <?php elseif($field instanceof Button): ?>
            <button
                <?php ### Tier 1 attributes : FormField attributes per inheritance ### ?>
                <?= ($field->getName() != null ? 'name="'.$field->getName().'"' : '') ?>
                <?= ($field->getId() != null ? 'id="'.$field->getId().'"' : '') ?>
                <?= ($field->getClass() != null ? 'class="'.$field->getClass().'"' : '') ?>
                <?= ($field->getDisabled() == true ? 'disabled="disabled"' : '') ?>

                <?php ### Tier 2 attributes : Button attributes ### ?>
                <?= ($field->getAutofocus() == true ? 'autofocus="autofocus"' : '') ?>
                <?= ($field->getFormaction() != null ? 'formaction="'.$field->getFormaction().'"' : '') ?>
                <?= ($field->getFormenctype() != null ? 'formenctype="'.$field->getFormenctype().'"' : '') ?>
                <?= ($field->getFormmethod() != null ? 'formmethod="'.$field->getFormmethod().'"' : '') ?>
                <?= ($field->getFormnovalidate() == true ? 'formnovalidate="formnovalidate"' : '') ?>
                <?= ($field->getFormtarget() != null ? 'formtarget="'.$field->getFormtarget().'"' : '') ?>
                <?= ($field->getType() != null ? 'type="'.$field->getType().'"' : '') ?>
            ><?= ($field->getValue() != null ? $field->getValue() : '') ?></button>
        <?php endif; ?>
    <?php endforeach ?>
<?php endif; ?>

</form>