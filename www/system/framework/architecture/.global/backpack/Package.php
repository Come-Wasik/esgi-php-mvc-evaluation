<?php

namespace Framework\Architecture\All\Component\Backpack;

use Framework\Architecture\All\Component\Interfaces\NamedBackpackInterface;

class Package implements NamedBackpackInterface
{
    private static $backpack = [];

    public static function set($name, $content)
    {
        self::$backpack[$name] = $content;
    }

    public static function get($name)
    {
        return self::$backpack[$name] ?? null;
    }

    public static function getAll()
    {
        return self::$backpack;
    }
}
