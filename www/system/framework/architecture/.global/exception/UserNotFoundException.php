<?php

namespace Framework\Architecture\All\Component\Exception;

use Exception;

class UserNotFoundException extends FormException
{
    public function __construct(string $message = '', int $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
