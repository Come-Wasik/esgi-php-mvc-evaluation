<?php

namespace Framework\Architecture\All\Component\Interfaces;

interface BDDInterface
{
    public function connexion(
        string $dbDriver,
        string $dbAddress,
        string $dbName,
        string $dbUser,
        string $dbPass
    );
}
