<?php

namespace Framework\Architecture\All\Component\Interfaces;

interface NamedBackpackInterface
{
    public static function set($name, $content);
    public static function get($name);
    public static function getAll();
}
