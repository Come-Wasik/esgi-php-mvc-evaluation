<?php

namespace Framework\Architecture\All\Component\Database;

use PDO;
use Framework\Architecture\All\Component\Interfaces\BDDInterface;

class PDOConnect implements BDDInterface
{
    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function connexion(
        string $dbDriver,
        string $dbAddress,
        string $dbName,
        string $dbUser,
        string $dbPass
    ) {
        $this->dbh = new PDO($dbDriver . ':dbname=' . $dbName . ';host=' . $dbAddress, $dbUser, $dbPass);
    }

    public function getConnexion()
    {
        return $this->dbh;
    }
}
