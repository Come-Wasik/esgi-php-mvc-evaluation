<?php

namespace Framework\Architecture\All\Component\Database;

class PostManager extends DatabaseManager
{
    public function getUserPost(int $id)
    {
        # !!! Initialisation faite dans /config/packageManager.php !!! #
        $connexionContainer = getInitializedPackage('PDOConnect');

        return (new QueryBuilder($connexionContainer))
            ->select('p.*, u.*')
            ->from('posts', 'p')
            ->join('users', 'u')
            ->where('p.author = :iduser')
            ->setParameter('iduser', $id)
            ->getQuery();
        // ->getArrayResult(Post::class);
    }
}

/**
 * p.* == posts.*
 * str_replace?
 */
