<?php

namespace Framework\Architecture\All\Component\Database;

use Exception;
use Framework\Architecture\All\Component\Interfaces\BDDInterface;
use Framework\Architecture\All\Component\Interfaces\ResultInterface;

class QueryBuilder
{
    private $dhb;
    private $request;
    private $aliases;
    private $arguments;

    public function __construct(BDDInterface $connection = null)
    {
        $this->dhb = $connection->getConnexion();
    }

    public function select(string $values = '*'): QueryBuilder
    {
        $selectParts = explode(', ', $values);

        foreach ($selectParts as $part) {
            list($table, $column) = explode('.', $part);
            $this->request['select'][] = [
                'table' => $table,
                'column' => $column
            ];
        }

        return $this;
    }

    public function from(string $table, string $alias): QueryBuilder
    {
        $this->request['from'] = $table;
        $this->aliases[$alias] = $table;
        return $this;
    }

    public function where(string $condition, string $logicalOperator = '&&'): QueryBuilder
    {
        # Init a conddition temporary container
        $newCondition = [];

        # Récupération des éléments comparés
        list($firstElement, $secondElement) = explode('=', $condition);

        # Suppression des espaces blanc
        $firstElement = trim($firstElement);
        $secondElement = trim($secondElement);

        list($table, $column) = explode('.', $firstElement);

        $newCondition = [
            'reference' => [
                'table' => $table,
                'column' => $column
            ], 'value' => $secondElement
        ];

        if (!empty($this->request['where'])) {
            $newCondition['logicalOperator'] = $logicalOperator;
        }

        $this->request['where'][] = $newCondition;

        return $this;
    }

    public function setParameter(string $key, string $value): QueryBuilder
    {
        $this->arguments[$key] = $value;
        return $this;
    }

    public function join(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        return $this->selectJoin('JOIN', $table, $aliasTarget, $fieldSource, $fieldTarget);
    }

    public function leftJoin(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        return $this->selectJoin('LEFT JOIN', $table, $aliasTarget, $fieldSource, $fieldTarget);
    }

    public function rightJoin(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        return $this->selectJoin('RIGHT JOIN', $table, $aliasTarget, $fieldSource, $fieldTarget);
    }

    public function innerJoin(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        return $this->selectJoin('INNER JOIN', $table, $aliasTarget, $fieldSource, $fieldTarget);
    }

    private function selectJoin(string $joinType, string $table, string $aliasTarget, string $fieldSource, string $fieldTarget)
    {
        $originTable = $this->request['from'];

        $this->request['join'][] = [
            'type' => $joinType,
            'table' => $table,
            'relation' => [
                $originTable => $fieldSource,
                $table => $fieldTarget
            ]
        ];

        $this->aliases[$aliasTarget] = $table;
        return $this;
    }

    public function addToQuery(string $query): QueryBuilder
    {
        # code...
        return $this;
    }

    public function getQuery(): ResultInterface
    {
        $request = '';
        $pdoArgs = $this->arguments;

        ### SELECT

        if (!key_exists('select', $this->request)) {
            throw new Exception('There is no SELECT in your Query Builder', 500);
        }
        $request .= 'SELECT ';

        foreach ($this->request['select'] as $selectPart) {
            $request .= $this->replace_alias($selectPart['table']) . '.' . $selectPart['column'] . ', ';
        }
        $request = rtrim($request, ', ');

        ### FROM
        if (!key_exists('from', $this->request)) {
            throw new Exception('There is no FROM in your Query Builder', 500);
        }
        $request .= ' FROM ' . $this->request['from'];

        ### INNER
        if (key_exists('join', $this->request)) {
            foreach ($this->request['join'] as $jointure) {
                $request .= ' ' . $jointure['type'] . ' ' . $this->replace_alias($jointure['table']) . ' ON ';
                foreach ($jointure['relation'] as $relation => $key) {
                    $request .= $relation . '.' . $key . ' = ';
                }
                $request = rtrim($request, '= ');
            }
        }

        ### WHERE
        if (key_exists('where', $this->request)) {
            $request .= ' WHERE ';
            foreach ($this->request['where'] as $whereClause) {
                if (key_exists('logicalOperator', $whereClause)) {
                    $request .= $whereClause['logicalOperator'] . ' ';
                }

                $request .= $this->replace_alias($whereClause['reference']['table']) . '.' . $whereClause['reference']['column'] . ' = ' . $whereClause['value'];
            }
        }

        $pdoSt = $this->dhb->prepare($request);
        if (!$pdoSt->execute($pdoArgs)) {
            throw new Exception($pdoSt->errorInfo()[0] . ' | ' . $pdoSt->errorInfo()[1] . ' | ' . $pdoSt->errorInfo()[2], 500);
        }

        $response = [];
        while ($data = $pdoSt->fetch()) {
            $response[] = $data;
        }

        dd($response);

        return new Result();
    }

    private function replace_alias($alias): string
    {
        if (key_exists($alias, $this->aliases)) {
            return $this->aliases[$alias];
        } else {
            return $alias;
        }
    }
}
