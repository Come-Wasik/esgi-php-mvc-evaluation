<?php

namespace Framework\Architecture\All\Component\Database;

use Framework\Architecture\All\Component\Interfaces\ResultInterface;

class Result implements ResultInterface
{
    private $result;

    public function setResult(array $result)
    {
    }

    public function getResult()
    {
        return $this->result;
    }
}
