<?php

namespace Framework\Architecture\All\Component\Database\Ressource;

use ReflectionClass;

trait HelperMethods
{
    /**
     * Appendix methods for Database Manager
     * 
     * @function getOnlyClassname = Delete namespace from fully qualified class name to return the result
     * @function getObjectAttrName = Renvoi la liste des arguments d'un objet (seulement les noms)
     * @function getObjectAttr = Renvoi la liste des arguments d'un objet sous forme clé/valeur => nom/valeur
     */

    // Thanks to codeFareith : https://coderwall.com/p/cpxxxw/php-get-class-name-without-namespace
    private function getOnlyClassname(string $className): string
    {
        return substr($className, strrpos($className, '\\') + 1);
    }

    private function getObjectAttr(object $object)
    {
        $attributeList = [];
        foreach($this->getObjectAttrName($object) as $attributeName) {
            $getter = 'get'.ucfirst($attributeName);
            $attributeList[$attributeName] = $object->$getter();
        }
        return $attributeList;
    }

    private function getObjectAttrName(object $object)
    {
        $ref = new ReflectionClass($object);
        $props = $ref->getProperties();
        return array_map(function($refAttr) {
            return $refAttr->getName();
        }, $props);
    }
}
