<?php

/**
 * A command used to launch a command. It can be filled by the user
 */

namespace Framework\Architecture\Console\CoreMaker\Stream;

use Exception;

class Command
{
	private $command;

	public function __construct(array $data = null)
    {
		### Hydratation ###
		if($data) {
			$this->hydrate($data);
		}
    }

    private function hydrate(array $data): void
    {
        foreach($data as $attName => $attValue) {
            $settername = 'set'. ucfirst($attName);
            $this->$settername($attValue);
        }
	}

	/**
	 * Setters and getters
	 */
	public function getCommand(): ?string
	{
		return $this->command;
	}

	public function setCommand(string $command): void
	{
		$this->command = $command;
	}

	/**
	 * Appendix methods
	 */
	public function fillFromGlobals(): void
	{
		$this->command = implode(' ', array_slice(($_SERVER['argv'] ?? $_ENV['argv'] ?? ''), 1));
	}

	public function getSize(): int
	{
		return (empty($this->command) ? 0 : 1 + substr_count($this->command, ' '));
	}
}
