<?php

namespace Framework\Architecture\Web\Component;

use Exception;

class Session
{
    public function session_start()
    {
        $status = \session_status();
        if($status == PHP_SESSION_DISABLED || $status == PHP_SESSION_NONE) {
            \session_start();
        } else {
            throw new Exception('La session est déjà démarrée', 500);
        }
    }

    public function session_stop()
    {
        $status = \session_status();
        if($status == PHP_SESSION_ACTIVE) {
            \session_destroy();
        } else {
            throw new Exception('La session n\'est pas démarrée', 500);
        }
    }

    public function set($dataName, $dataContent)
    {
        $status = \session_status();
        if($status == PHP_SESSION_ACTIVE) {
            $_SESSION[$dataName] = $dataContent;
        } else {
            throw new Exception('La session n\'est pas démarrée', 500);
        }
    }

    public function get($dataName)
    {
        $status = \session_status();
        if($status == PHP_SESSION_ACTIVE) {
            return $_SESSION[$dataName] ?? null;
        } else {
            throw new Exception('La session n\'est pas démarrée', 500);
        }
    }
}
