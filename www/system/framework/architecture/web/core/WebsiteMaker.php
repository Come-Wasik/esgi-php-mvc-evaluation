<?php

namespace Framework\Architecture\Web\CoreMaker;

use Framework\Architecture\Web\Routage\RouteManager;
use Framework\Architecture\Web\CoreMaker\Component\Http\Response;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;
use Framework\Architecture\Web\Security\Authentification;
use Exception;

class WebsiteMaker
{
    public function __construct()
    {
        $this->getRouteFiles();
    }

    public function getRouteFiles()
    {
        ### Remplissage des routes ###
        ## Récupération de la liste des fichiers de route par architecture (web, console) ##
        $webRouteFilenames = getConfig('router')['filesFor']['web'];
        $webRoutesPath = array_map(function ($filename) {
            if (!is_string($filename)) {
                throw new Exception('One route filename in the data storage for router isn\'t a string');
            }
            return getConfig('route_path') . '/' . $filename . '.php';
        }, $webRouteFilenames);

        // Récupération de toutes les routes
        foreach ($webRoutesPath as $routeFile) {
            if (!file_exists($routeFile)) {
                throw new Exception('The filename : ' . $routeFile . ' doens\'t exist', 500);
            }
            include $routeFile;
        }
    }

    public function processRequest(Request $request)
    {
        ### Authentification starting ###
        $authManager = new Authentification();
        $authManager->startSystem($request);

        ### Routage starting ###
        ## Route searching ##
        $manager = new RouteManager();
        $route = $manager->searchRoute($request);

        # Enregistrement temporaire de l'action à effectuer pour des tests #
        $action = $route->getAction();

        # Test de capacité d'interprétation #
        if (!is_callable($action) && !is_string($action)) {
            throw new Exception('The action from the route ' . $route->getUrl() . ' cannot be process.', 500);
        }

        ## Execution de l'action ##
        if (is_callable($action)) {
            # Injection des dépendences
            $inputArgList = get_callback_args($action);
            $argsToSend = [];

            if (!empty($inputArgList)) {
                $argsToSend = $this->fillActionArguments($inputArgList, $route->getVariables(), $request);
            }

            ### Lancement de l'action requêté / Lancement d'un callable ###
            $response = $action(...$argsToSend);

            ### Renvoi de la réponse généré sous forme d'objet valide "Response" ###
            return $this->filterResponseFromController($response);
        } elseif (is_string($action)) {
            ### Traitements -- Récupération d'un controller et de sa méthode ###
            if (
                empty($action)
                || \substr_count($action, '@') != 1
                || \strpos($action, '@', 1) === false
                || \strlen($action) < 3
            ) {
                throw new Exception('Route action ' . $action . ' cannot be read', 500);
            }

            # Récupération du controller et de la méthode
            $actionContent = explode('@', $action);

            if (
                count($actionContent) != 2
                || empty($actionContent[1])
            ) {
                throw new Exception('Action is not correct. please use notation \'myController@myMethod\'', 500);
            }

            # Récupération du contrôleur et de sa méthode à lancer
            $controller = $actionContent[0];
            $method = $actionContent[1];

            if (!class_exists($controller)) {
                throw new Exception('The controller ' . $controller . ' not exists', 500);
            }

            if (!method_exists($controller, $method)) {
                throw new Exception('The method ' . $method . ' not exists', 500);
            }

            # Injection des dépendences demandées
            $inputArgList = get_method_args($controller, $method);
            $argsToSend = [];

            if (!empty($inputArgList)) {
                $argsToSend = $this->fillActionArguments($inputArgList, $route->getVariables(), $request);
            }

            ### Lancement de l'action requêté ## Lancement d'un controller ###
            $response = (new $controller())->$method(...$argsToSend);

            ### Renvoi de la réponse généré sous forme d'objet valide "Response" ###
            return $this->filterResponseFromController($response);
        }
    }

    public function sendResponse(Response $response)
    {
        $response->print();
    }

    private function filterResponseFromController($response)
    {
        if ($response != null) {
            if ($response instanceof Response) {
                return $response;
            } elseif (is_string($response)) {
                return new Response($response);
            } else {
                throw new Exception('Content returned by the callable cannot be sended.', 500);
            }
        } else {
            return new Response();
        }
    }

    public function fillActionArguments(array $inputArgList, array $routeVariables, Request $request)
    {
        foreach ($inputArgList as $argName => $argData) {
            # Récupération des informations de cet argument
            $argType = $argData['type'];

            # Recherche d'une variable avec ce nom
            if (array_key_exists($argName, $routeVariables)) {
                # Une des variables de la route correspond à l'argument demandé
                $argsToSend[] = $routeVariables[$argName];

            # Recherche d'une dépendance ayant le même type que demandé
            } elseif (\getPackage($argType) !== null) {
                $packageData = \getPackage($argType);

                # On tente de récupérer une instance du package. Elle est normalement initialisée dans le fichier config/packages
                if (key_exists('newInstance', $packageData)) {
                    $packageInitialized = $packageData['newInstance']();

                # Sinon, on instancie la classe sans arguments
                } else {
                    $packageInitialized = new $argType();
                }
                $argsToSend[] = $packageInitialized;
            } elseif ($argType == Request::class) {
                $argsToSend[] = $request;
            } else {
                throw new Exception('Try to get argument ' . $argName . ' but it is unknown for the requested route', 500);
            }
        }
        return $argsToSend;
    }
}
