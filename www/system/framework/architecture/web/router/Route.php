<?php

namespace Framework\Architecture\Web\Routage;

class Route
{
    private $name;
    private $url;
    private $method;
    private $action;
    private $variables;

    public function __construct(array $data)
    {
        $this->variables = [];
        $this->hydrate($data);
    }

    private function hydrate(array $data)
    {
        foreach ($data as $attName => $attValue) {
            $settername = 'set' . ucfirst($attName);
            $this->$settername($attValue);
        }
    }

    /**
     * Setters and getters
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getVariables()
    {
        return $this->variables;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    public function setAction($action): void
    {
        $this->action = $action;
    }

    public function setVariables(array $variables): void
    {
        $this->variables = $variables;
    }

    /**
     * Appendix setters
     */
    public function addVariable(string $varName, string $varData): void
    {
        $this->variables[$varName] = $varData;
    }

    /**
     * Appendix research method
     */
    public function getVariableListInUrl(): array
    {
        $varsInPath = [];
        $urlTrimed = trim($this->getUrl(), '/');
        $urlParts = explode('/', $urlTrimed);

        for ($i = 0; $i < count($urlParts); $i++) {
            # On teste si sur la partie d'url en cours, il y a une variable
            if (
                strpos($urlParts[$i], '{', 0) !== false
                && strpos($urlParts[$i], '}', strlen($urlParts[$i]) - 1) !== false
            ) {
                # Enregistrement de l'id et du nom de la variable
                $varsInPath[$i] = substr($urlParts[$i], 1, strlen($urlParts[$i]) - 2);
            }
        }
        return $varsInPath;
    }
}
