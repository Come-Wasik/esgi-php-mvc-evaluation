<?php

namespace Framework\Architecture\Web\Security;

use Framework\Architecture\All\Component\Exception\UserNotFoundException;
use Framework\Architecture\All\Component\Exception\FormException;
use Packages\FormMaker\Validator\Validator;
use Exception;
use Framework\Architecture\Web\CoreMaker\Component\Http\Request;

class Authentification
{
    private $userClass;
    private $userTable;
    private $roleClass;
    private $roleTable;
    private $DatabaseManager;

    public function __construct()
    {
        $securityConfiguration = getConfig('security');

        if ($securityConfiguration['moduleEnabled']) {
            ### Variable definition ###
            $this->userClass = $securityConfiguration['userEntity'];
            $this->userTable = $this->getOnlyClassname($this->userClass);
            $this->roleClass = $securityConfiguration['roleEntity'];
            $this->roleTable = $this->getOnlyClassname($this->roleClass);
            $this->DatabaseManager = getInitializedPackage('DatabaseManager');
        }
    }

    public function startSystem(Request $request): void
    {
        $securityConfiguration = getConfig('security');

        if ($securityConfiguration['moduleEnabled']) {
            ### Processing ###
            try {
                ## Connexion ##
                if ($request->getUri() == $securityConfiguration['connexionData']['route']) {
                    if ($this->userIsConnnected()) {
                        redirection($securityConfiguration['connexionData']['redirectionAfterProcessing']);
                    } else {
                        if ($request->getHttpMethod() == $securityConfiguration['connexionData']['httpMethod']) {
                            ## Getting all user input data ##
                            if ($securityConfiguration['connexionData']['httpMethod'] == 'GET') {
                                $userInput = $request->getQuery();
                            } elseif ($securityConfiguration['connexionData']['httpMethod'] == 'POST') {
                                $userInput = $request->getRequest();
                            }

                            # Primary tests (data existing, expected amount of data, good types) #
                            $validator = new Validator();
                            if (($message = $validator->searchError('user_login', $request)) !== null) {
                                throw new Exception($message, 500);
                            }

                            # Obtaining all data with XSS filtering #
                            $credentials = [
                                'username' => htmlentities($userInput['email']),
                                'email' => htmlentities($userInput['email']),
                                'password' => htmlentities($userInput['password'])
                            ];

                            $this->tryConnexion($credentials);
                            # Redirection de l'utilisateur vers la page deconnexion "AfterProcessing" #
                            redirection($securityConfiguration['connexionData']['redirectionAfterProcessing']);
                        }
                    }
                } elseif ($request->getUri() == $securityConfiguration['registrationData']['route']) {
                    ## Registration ##
                    if ($this->userIsConnnected()) {
                        redirection($securityConfiguration['registrationData']['redirectionAfterProcessing']);
                    } else {
                        if ($request->getHttpMethod() == $securityConfiguration['registrationData']['httpMethod']) {
                            ## Getting all user input data ##
                            if ($securityConfiguration['connexionData']['httpMethod'] == 'GET') {
                                $userInput = $request->getQuery();
                            } elseif ($securityConfiguration['connexionData']['httpMethod'] == 'POST') {
                                $userInput = $request->getRequest();
                            }

                            ### Vérification de l'existance des champs + protection XSS avec htmlentities + rejet d'un champ inconnu
                            $dataNeeded = [
                                'obligation' => [
                                    'username',
                                    'password',
                                    'passwordConfirm',
                                    'email',
                                ],
                                'optionnal' => [
                                    'firstname',
                                    'lastname',
                                ],
                            ];
                            ## Analyse des champs obligatoires
                            foreach ($dataNeeded['obligation'] as $keyKnown) {
                                if (!key_exists($keyKnown, $userInput)) {
                                    throw new FormException('La donnée ' . $keyKnown . ' manque', 500);
                                } else {
                                    $keyKnown = htmlentities($userInput[$keyKnown]);
                                }
                            }
                            ## Analyse des champs optionnels
                            foreach ($dataNeeded['optionnal'] as $keyKnown) {
                                if (key_exists($keyKnown, $userInput)) {
                                    $keyKnown = htmlentities($userInput[$keyKnown]);
                                }
                            }
                            ## Recherche d'un champ ajouté involontairement
                            foreach ($userInput as $key => $data) {
                                if (!in_array($key, $dataNeeded['obligation']) && !in_array($key, $dataNeeded['optionnal'])) {
                                    throw new FormException('Le champ ' . $key . ' n\'est pas autorisé', 500);
                                }
                            }

                            ## Vérification du contenu des champs
                            # Vérification du mot de passe
                            if ($userInput['password'] != $userInput['passwordConfirm']) {
                                throw new FormException('Les mots de passe ne correspondent pas', 500);
                            }
                            unset($userInput['passwordConfirm']);

                            #  Vérification de l'email
                            if (filter_var($userInput['email'], FILTER_VALIDATE_EMAIL) === false) {
                                throw new FormException('L\'email est invalide', 500);
                            }

                            ## Saisie du rôle
                            $DatabaseManager = $this->DatabaseManager;
                            $roleTable = $this->roleTable;
                            $userInput['role_id'] = $DatabaseManager->request('SELECT id FROM ' . $roleTable . " WHERE name = 'Simple user'")[0]['id'];

                            ## Saisie du status
                            $userInput['status'] = 'enabled';

                            ## Saisie de la date d'inscription
                            $userInput['date_registration'] = getFormattedTimestamp();

                            # Try to register #
                            $this->tryRegistration([
                                'username' => $userInput['username'],
                                'password' => $userInput['password'],
                                'email' => $userInput['email'],
                                'firstname' => $userInput['firstname'],
                                'lastname' => $userInput['lastname'],
                                'role_id' => $userInput['role_id'],
                                'status' => $userInput['status'],
                                'date_registration' => $userInput['date_registration'],
                            ]);

                            # If admin say that user must be connected after registration ... #
                            if ($securityConfiguration['registrationData']['connexionAfterRegistration']) {
                                # Obtaining all data with XSS filtering #
                                $credentials = [
                                    'username' => $userInput['username'],
                                    'email' => $userInput['email'],
                                    'password' => $userInput['password']
                                ];

                                $this->tryConnexion($credentials);
                            }

                            # Redirection de l'utilisateur vers la page d'inscription "AfterProcessing" #
                            redirection($securityConfiguration['registrationData']['redirectionAfterProcessing']);
                        }
                    }
                } elseif ($request->getUri() == $securityConfiguration['deconnexionData']['route']) {
                    ## Deconnexion ##
                    if ($this->userIsConnnected()) {
                        $this->deconnexion();
                    }
                    # Redirection de l'utilisateur vers la page de déconnexion "AfterProcessing" #
                    redirection($securityConfiguration['deconnexionData']['redirectionAfterProcessing']);
                } else {
                    ## Renouvellement de la connexion ##
                    if ($this->userIsConnnected()) {
                        $this->tryReconnexion();
                    }
                }
            } catch (FormException $error) {
                setSession('security.form_error', $error->getMessage());
            }
        }
    }

    public function userIsConnnected(): bool
    {
        ### Si le token existe (dans la session) ###
        if (getSession('session_token')) {
            return true;
        }
        return false;
    }

    private function tryConnexion(array $credentials): void
    {
        ### Requétage de l'utilisateur ###
        $userClass = $this->userClass;
        $userTable = $this->userTable;
        $roleTable = $this->roleTable;
        $DatabaseManager = $this->DatabaseManager;

        $user = $DatabaseManager->request(
            'SELECT * FROM ' . $userTable . ' WHERE username = :username OR email = :email AND password = :password',
            $credentials,
            'class',
            [
                'classname' => $userClass
            ]
        );

        if (is_array($user) && !empty($user)) {
            $user = $user[array_key_first($user)];
        }

        if (!$user) {
            throw new UserNotFoundException('User dont\'t exists');
        }

        if ($user->getStatus() != 'enabled') {
            throw new UserNotFoundException('User in not enabled');
        }

        ### Token generation ###
        $userRole = $DatabaseManager->request('SELECT name FROM ' . $roleTable . ' WHERE id = :id', ['id' => $user->getRole_id()])[0]['name'];

        $token = $this->generateToken([
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'role' => $userRole,
        ]);

        ### Integrate token in session ###
        setSession('session_token', $token);

        ### Register token and connexion date in database ###
        $user->setSession_token(addslashes($token));
        $user->setLast_connexion(getFormattedTimestamp());
        $DatabaseManager->update($user);
    }

    public function tryRegistration(array $userData): void
    {
        ### Requétage de l'utilisateur ###
        $userClass = $this->userClass;
        $DatabaseManager = $this->DatabaseManager;

        $newUser = new $userClass();
        # Appel automatique des setters
        foreach ($userData as $key => $data) {
            $setter = 'set' . ucfirst($key);
            $newUser->$setter($data);
        }

        $DatabaseManager->add($newUser);
    }

    private function tryReconnexion(): void
    {
        $roleTable = $this->roleTable;

        ### On récupère le token ###
        $token = getSession('session_token');
        $tokenDecoded = \json_decode($token, 1);

        ### On vérifie la véracité de la signature ###
        $recreatedSign = hash('sha512', \json_encode($tokenDecoded['header']) . '.' . \json_encode($tokenDecoded['payload']));
        if ($tokenDecoded['sign'] == $recreatedSign) {
            ### On vérifie que le token est bien le même que dans l'objet utilisateur ###
            $userClass = $this->userClass;
            $DatabaseManager = $this->DatabaseManager;
            $user = $DatabaseManager->get($userClass, $tokenDecoded['payload']['id']);

            $tokenInDb = stripcslashes($user->getSession_token());

            if (!$user) {
                throw new UserNotFoundException('User dont\'t exists');
            }

            if ($user && $tokenInDb == $token) {
                ### Token generation ###
                $userRole = $DatabaseManager->request('SELECT name FROM ' . $roleTable . ' WHERE id = :id', ['id' => $user->getRole_id()])[0]['name'];

                $newToken = $this->generateToken([
                    'id' => $user->getId(),
                    'username' => $user->getUsername(),
                    'role' => $userRole,
                ]);

                ### Integrate token in session ###
                setSession('session_token', $newToken);

                ### Register token and connexion date in database ###
                $user->setSession_token(addslashes($newToken));
                $user->setLast_connexion(getFormattedTimestamp());
                $DatabaseManager->update($user);
            } else {
                ### On déconnecte l'utilisateur ###
                session_stop();
            }
        } else {
            ### On déconnecte l'utilisateur ###
            session_stop();
        }
    }

    private function deconnexion(): void
    {
        ## Stop session ##
        session_stop();
    }

    private function generateToken(array $user): string
    {
        ### Création d'un tableau comprenant les informations primordiales ###
        $tokenData = [
            'header' => [
                'typ' => 'JWT',
                'algo' => 'FALSE'
            ],
            'payload' => [
                'id' => $user['id'],
                'username' => $user['username'],
                'role' => $user['role'],
                'horodatage' => getTimestamp(),
                'TTL' => getTimestamp() + (60 * 5),
            ],
        ];

        ### Génération de la signature du token ###
        $tokenData['sign'] = hash('sha512', \json_encode($tokenData['header']) . '.' . \json_encode($tokenData['payload']));

        ### Renvoi du token préparé
        return \json_encode($tokenData);
    }

    // Thanks to codeFareith : https://coderwall.com/p/cpxxxw/php-get-class-name-without-namespace
    private function getOnlyClassname(string $className): string
    {
        return substr($className, strrpos($className, '\\') + 1);
    }
}
