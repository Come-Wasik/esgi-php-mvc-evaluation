<?php

namespace Framework\Bootstrap\FileLoader;

use Framework\Architecture\All\Component\Backpack\Config as ConfigBackpack;

class Config implements FileLoaderInterface
{
    public function init(): void
    {
        ### Importing make.ini data ###
        $makeIniPath = base_path().'/maker.ini';
        if(!file_exists($makeIniPath)) {
            die('Fatal error : makerIni file should exists');
        }
        $makerData = parse_ini_file($makeIniPath);

        ### Définition du dossier de stockage des données de configuration ###
        $dataStoragePath = base_path().'/'.($makerData['data_storage_path'] ?? 'config/dataStorage');
        $configPathPattern = $dataStoragePath.'/*.php';

        ### Fonctionnement de test -- Obtention de toutes les données
        if(defined('ENV') && ENV == 'test') {
            ## Enregistrement de toutes les données ##
            $datafileList = $this->searchSubFilesInDir($configPathPattern);
            $this->registerDataInBackpack($datafileList);
        ### Fonctionnement normal -- Obtention des données globales et d'environnement ###
        } else {
            ## Enregistrement des données ##
            $datafileList = glob($configPathPattern);
            $this->registerDataInBackpack($datafileList);
        }
    }

    private function registerDataInBackpack(array $configFileList): void
    {
        foreach($configFileList as $file) {
            # Affectation de chaque données trouvés dans chaque fichier config au backpack (sac à dos/conteneur) config
            foreach(include $file as $dataName => $dataContent) {
                ConfigBackpack::set($dataName, $dataContent);
            }
        }
    }

    ## Enregistrement d'une fonction de parcourt de fichiers dans un dossier et ses sous-dossiers ##
    private function searchSubFilesInDir(string $dirToResearch): array
    {
        $fileList = [];
        foreach(glob($dirToResearch) as $file) {
            if(is_dir($file)) {
                # Recherche inclusive avec fusion du résultat
                $fileList = array_merge($fileList, $this->searchSubFilesInDir($file.'/*'));
            } else {
                ## Enregistrement du fichier
                if(strpos($file, '.php', strlen($file) - 4)) {
                    $fileList[] = $file;
                }
            }
        }
        return $fileList;
    }
}
