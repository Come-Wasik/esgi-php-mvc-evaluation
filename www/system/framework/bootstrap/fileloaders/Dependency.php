<?php

namespace Framework\Bootstrap\FileLoader;

use Framework\Architecture\All\Component\Backpack\Package as PackageBackpack;

class Dependency implements FileLoaderInterface
{
    public function init(): void
    {
        ### Importing make.ini data ###
        $makeIniPath = base_path() . '/maker.ini';
        if (!file_exists($makeIniPath)) {
            die('Fatal error : makerIni file should exists');
        }
        $makerData = parse_ini_file($makeIniPath);

        ### Définition du dossier de stockage des données de configuration ###
        $injDepFilePath = base_path() . '/' . ($makerData['config_path'] ?? 'config/') . '/packageManager.php';

        # Affectation de chaque données dans le conteneur (ou backpack)
        foreach (include $injDepFilePath as $dataName => $dataContent) {
            PackageBackpack::set($dataName, $dataContent);
        }
    }
}
