<?php

namespace Framework\Bootstrap\FileLoader;

interface FileLoaderInterface
{
    public function init(): void;
}
