<?php

namespace Framework\Bootstrap\FileLoader;

use Framework\Architecture\All\Component\Backpack\Config as ConfigBackpack;

class Helper implements FileLoaderInterface
{
    public function init(): void
    {
        ### Définition du dossier de stockage des données de configuration ###
        $helperpath = ConfigBackpack::get('helper_path');
        $helperPathPattern = $helperpath.'/*.php';

        ### Fonctionnement normal -- Obtention des helpers globaux et d'architecture ###
        $dataFileList = $this->searchSubFilesInDir($helperpath.'/*');
        $this->registerHelpers($dataFileList);
    }

    private function registerHelpers(array $helperList): void
    {
        foreach($helperList as $file) {
            include $file;
        }
    }

    ## Enregistrement d'une fonction de parcourt de fichiers dans un dossier et ses sous-dossiers ##
    private function searchSubFilesInDir(string $dirToResearch): array
    {
        $fileList = [];
        foreach(glob($dirToResearch) as $file) {
            if(is_dir($file)) {
                # Recherche inclusive avec fusion du résultat
                $fileList = array_merge($fileList, $this->searchSubFilesInDir($file.'/*'));
            } else {
                ## Enregistrement du fichier
                if(strpos($file, '.php', strlen($file) - 4)) {
                    $fileList[] = $file;
                }
            }
        }
        return $fileList;
    }
}
