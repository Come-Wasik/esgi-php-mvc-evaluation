<?php

$default_timezone = getConfig('app')['default_timezone'] ?? 'Europe/London';
date_default_timezone_set($default_timezone);