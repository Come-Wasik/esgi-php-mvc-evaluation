<?php

if(!function_exists('dprint')) {
    function dprint($data)
    {
        if(ENV == 'dev') {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
        }
    }
}

if(!function_exists('dd')) {
    function dd($data)
    {
        if(ENV == 'dev') {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
            die();
        }
    }
}

if(!function_exists('ddump')) {
    function ddump($data)
    {
        if(ENV == 'dev') {
            echo '<pre>';
            var_dump($data);
            echo '</pre>';
        }
    }
}

if(!function_exists('base_path')) {
    function base_path()
    {
        return (!empty($_SERVER['DOCUMENT_ROOT']) ? dirname($_SERVER['DOCUMENT_ROOT']) : dirname(dirname(dirname(dirname(__DIR__)))));
    }
}