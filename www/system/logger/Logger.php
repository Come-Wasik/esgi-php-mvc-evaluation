<?php

/**
 * Logger - A tool to set and get logs messages
 * @author SecretCanvas
 */

namespace Packages\Logger;

use Error;

class Logger {
    const ADVICE = 1;
    const NOTICE = 2;
    const DEPRECIED = 3;
    const WARNING = 4;
    const FATAL_ERROR = 5;

    public static $logBackpack = [];

    public static function setLog(string $message, int $code): void
    {
        if($code < self::ADVICE || $code > self::FATAL_ERROR) {
            throw new \Exception('Cannot set a log with '.$code.' error code.');
        }
        self::$logBackpack[] = [
            'message' => $message,
            'code' => $code
        ];

        if($code == self::FATAL_ERROR) {
            throw new Error('Logger has thrown a FATAL_ERROR');
        }
    }

    public function getLogs(): array
    {
        return self::$logBackpack;
    }

    public function getLogsByCode(int $code)
    {
        $logs = null;   ## Init value for logs with the asked code
        if(empty(self::$logBackpack)) { ## Cannot enter in foreach if logBackpack is empty
            foreach(self::$logBackpack as $currentLog) {
                if($currentLog['code'] == $code) {
                    $logs[] = $currentLog;
                }
            }
        }
        return $logs;
    }

    public function printLogs()
    {
        $logs = $this->getLogs();

        if(!empty($logs)) { ## LogList is not empty or null
            echo 'Log(s) registered : ';
            foreach($logs as $log) { ## Show each log
                ## Get the name for a code
                $classRef = new \ReflectionClass(\get_class($this));
                $constants = $classRef->getConstants();
                $codeName = array_search($log['code'], $constants);

                echo "\n".'Code '.$log['code'].' - '.$codeName.' : '.$log['message'];
            }
            echo "\n";
        }
    }
}